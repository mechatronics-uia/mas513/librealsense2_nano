## Building from Source using **Native** Backend for UiA bot with QEngineering 20.04 image

⮕ Use the V4L Native backend by applying the kernel patching

* **Prerequisite**

  * Verify internet connection.  
  * Verify the available space on flash, the patching process requires **~2.5Gb** free space  
    
    >df -h
  * Configure the Jetson Board into Max power mode (desktop -> see the upper right corner)  
  * Disconnect attached USB/UVC cameras (if any).  

* **Build and Patch Kernel Modules for Jetson L4T**

  * Clone librealsense repo into `Documents` folder and checkout release-tag v2.51.1

    ```sh
    git clone https://github.com/IntelRealSense/librealsense.git
    cd librealsense
    git checkout v2.51.1
    ```

  * Navigate to the root of librealsense directory.  
  * Run the script (note the ending characters - `L4T`)

    ```sh
    ./scripts/patch-realsense-ubuntu-L4T.sh  
    ```

  * The script will run for about 10 minutes depending on internet speed and perform the following tasks:

    a. Fetch the kernel source trees required to build the kernel and its modules.  
    b. Apply Librealsense-specific kernel patches and build the modified kernel modules.  
    c. Try to insert the modules into the kernel.

    ![d400](./img/kernel_patch_L4T.png)

* **Build librealsense SDK** 

  * Copy the three lines below to the end of your `.bashrc` file and source it afterwards:

    ```sh
    export CUDA_HOME=/usr/local/cuda
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64:/usr/local/cuda/extras/CUPTI/lib64
    export PATH=$PATH:$CUDA_HOME/bin
    ```
  * Make sure you are still in librealsense root directory 
  * Install necessary packages, setup udev rules and finally build+install with cmake:

    ```sh
    sudo apt-get install libssl-dev libusb-1.0-0-dev libudev-dev pkg-config libgtk-3-dev -y
    ./scripts/setup_udev_rules.sh  
    mkdir build && cd build  
    cmake .. -DBUILD_EXAMPLES=true -DCMAKE_BUILD_TYPE=release -DFORCE_RSUSB_BACKEND=false -DBUILD_WITH_CUDA=true && make -j$(($(nproc)-1)) && sudo make install
    ```

  The CMAKE `-DBUILD_WITH_CUDA=true` flag assumes CUDA modules are installed, which it is on UiAbot image.

  * This will take som time so go grab a coffe cup or two (about 2 hours).
  
    
* **Connect Realsense Device, run `realsense-viewer` and inspect the results**

* After SDK is veryfied: clone, build and run realsense-ros packages using this [guide](realsense-ros-install.md)
